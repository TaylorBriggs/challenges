import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Overlay, Popover } from 'react-bootstrap';

class ActionPopover extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPopover: false,
    };
  }

  togglePopover = () => {
    this.setState({
      showPopover: !this.state.showPopover,
    });
  }

  closePopover = () => {
    this.setState({
      showPopover: false,
    });
  }

  onSubmit = () => {
    this.props.submitCallback();
    this.closePopover();
  }

  render() {
    return (
      <span>
        <Button
          ref={(el) => {this.targetButton = el}}
          bsStyle={this.props.buttonStyle}
          onClick={() => this.togglePopover(!this.state.showPopover)}
        >
          {this.props.buttonLabel}
        </Button>

        <Overlay
          show={this.state.showPopover}
          onHide={() => this.togglePopover(false)}
          rootClose
          placement="bottom"
          target={this.targetButton}
        >
          <Popover 
            id={`action-popover-${this.props.id}`}
            className="bridge-popover"
          >
            <div className="row">
              <div className="col-xs-12">
                <p>{this.props.text}</p>

                <Button
                  className="center-block"
                  onClick={this.onSubmit}
                >
                  {this.props.actionLabel}
                </Button>
              </div>
            </div>
          </Popover>
        </Overlay>
      </span>
    );
  }
};

ActionPopover.propTypes = {
  text: PropTypes.string,
  actionLabel: PropTypes.string,
  submitCallback: PropTypes.func.isRequired,
  buttonLabel: PropTypes.string.isRequired,
  bsStyle: PropTypes.string,
};

ActionPopover.defaultProps = {
  text: '',
  buttonLabel: 'Submit',
  buttonStyle: 'link',
};

export default ActionPopover;