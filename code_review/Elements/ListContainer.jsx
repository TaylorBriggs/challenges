import React from 'react';
import PropTypes from 'prop-types';

const ListContainer = (props) => {
	return (
		<div className="card-large card-list">
			<div className="card-color-header">
				<h2 className="all-caps-text">{props.title}</h2>
			</div>
			{props.children}
		</div>
	);
}

ListContainer.propTypes = {
	title: PropTypes.string.isRequired,
};

export default ListContainer;