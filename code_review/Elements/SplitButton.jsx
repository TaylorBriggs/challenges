import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Button, Overlay, Popover } from 'react-bootstrap';

import LoadingIndicator from './LoadingIndicator.jsx';

// pass in array of dropdown items with label, aria-label, onClick, custom class for each
// loop through each

class SplitButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPopover: false,
      isLoading: this.props.isLoading,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.state.isLoading !== nextProps.isLoading) {
      this.setState({
        isLoading: nextProps.isLoading,
      })
    }
  }

  togglePopover = () => {
    this.setState({
      showPopover: !this.state.showPopover,
    });
  }

  closePopover = () => {
    this.setState({
      showPopover: false,
    });
  }

  getButton = () => {
    if (this.props.buttonHref) {
      return (
        <Button
          href={this.props.buttonHref}
          disabled={this.props.disabled}
          id={`split-${this.props.id}`}
        >
          {this.state.isLoading ?
            <LoadingIndicator small />
          :
            this.props.buttonLabel
          }
        </Button>
      );
    } else {
      return (
        <Button
          ref={(el) => {this.primaryButton = el}}
          onClick={this.props.overlay ? () => this.togglePopover(!this.state.showPopover) : this.props.buttonClick}
          disabled={this.props.disabled}
          id={`split-${this.props.id}`}
        >
          {this.state.isLoading ?
            <LoadingIndicator small />
          :
            this.props.buttonLabel
          }
        </Button>
      );
    }
  }

  render() {
    return (
      <div className="btn-split">
        <Dropdown id={`split-button-${this.props.id}`}>
            {this.getButton()}

            {this.props.overlay ?
              <Overlay
                show={this.state.showPopover}
                onHide={() => this.togglePopover(false)}
                rootClose
                placement="bottom"
                target={this.primaryButton}
              >
                <Popover
                  id={`primary-btn-popover-${this.props.id}`}
                  className={`bridge-popover${this.props.overlaySmall ? " bridge-popover-sm" : ""}`}
                >
                 {React.cloneElement(this.props.overlay, {closeCallback: this.closePopover})}
                </Popover>
              </Overlay>
            : null}
          <Dropdown.Toggle noCaret>
            <i className="fa fa-lg fa-chevron-down" />
          </Dropdown.Toggle>

          <Dropdown.Menu>
            {this.props.children}
          </Dropdown.Menu>
        </Dropdown>
      </div>
    );
  }
};

SplitButton.propType = {
  id: PropTypes.number.isRequired,
  buttonLabel: PropTypes.string.isRequired,
  buttonClick: PropTypes.func,
  buttonHref: PropTypes.string,
  customClass: PropTypes.string,
  disabled: PropTypes.bool,
  overlay: PropTypes.object,
  isLoading: PropTypes.bool,
  overlaySmall: PropTypes.bool,
};

SplitButton.defaultProps = {
  buttonClick: null,
  buttonHref: null,
  customClass: null,
  disabled: false,
  overlay: null,
  isLoading: false,
  overlaySmall: false,
};

export default SplitButton;
