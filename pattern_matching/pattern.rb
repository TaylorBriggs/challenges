class PatternMatching
  def follows_pattern?(pattern, input)
    pattern_map = create_pattern_map(pattern)
    chunks = chunk(input, pattern.length)

    pattern_map.all? do |_part, indices|
      selected = chunks.select.with_index do |_item, index|
        indices.include? index
      end
      selected.uniq.length == 1
    end
  end

  private

  def create_pattern_map(pattern)
    positions = {}
    pattern.each_char.with_index do |char, index|
      positions[char] ||= []
      positions[char] << index
    end
    positions
  end

  def chunk(input, num_chunks)
    chunk_size = input.length / num_chunks

    if (input.length % num_chunks).zero?
      input.scan(/.{1,#{chunk_size}}/)
    else
      odd_chunks(input, num_chunks, chunk_size)
    end
  end

  def odd_chunks(input, num_chunks, min_size)
    max_size = min_size + 1
    chunks = []
    offset = 0

    num_chunks.times do |i|
      size = i.even? ? min_size : max_size
      chunks << input[offset, size]
      offset += size
    end

    chunks
  end
end
